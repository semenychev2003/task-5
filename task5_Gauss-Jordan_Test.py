import random

import numpy as np
import sys
import time
import matplotlib.pyplot as plt


def gauss(equation_amount):
    n = equation_amount
    a = np.zeros((n, n + 1))
    x = np.zeros(n)

    for i in range(n):
        for j in range(n + 1):
            a[i][j] = random.uniform(1.0, 100.0)


    for i in range(n):
        if a[i][i] == 0.0:
            sys.exit('Деление на ноль!')

        for j in range(n):
            if i != j:
                ratio = a[j][i] / a[i][i]

                for k in range(n + 1):
                    a[j][k] = a[j][k] - ratio * a[i][k]

    for i in range(n):
        x[i] = a[i][n] / a[i][i]

    for i in range(n):
        print('X%d = %0.2f' % (i, x[i]), end='\t')


times = []
sizes = []

for i in range(1, 50):
    sizes.append(i)
    time_start = time.perf_counter()
    print(gauss(i))
    time_end = time.perf_counter()
    times.append(time_end - time_start)

plt.plot(sizes, times)
plt.xlabel('Количество уравнений')
plt.ylabel('Время вычисления, с')
plt.legend()
plt.show()
