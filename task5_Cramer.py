import numpy as np
import numpy.linalg


def cramer(equation_number):
    input_a = [[int(input(f'Введите коэффициент {n+1} уравнения {f+1} - ')) for n in range(equation_number)] for f in range(equation_number)]
    input_b = [int(input(f'Введите правую часть уравнения {n} - ')) for n in range(equation_number)]
    a = np.array(input_a)
    b = np.array(input_b)
    try:
        x = np.linalg.solve(a, b)
        return x
    except numpy.linalg.LinAlgError:
        print('Корней нет')


print(cramer(equation_number=int(input('Введите количество уравнений - '))))
